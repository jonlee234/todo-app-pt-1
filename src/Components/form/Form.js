import React from "react"
import todosList from "/todos.json"
import App from "../../App";

export default function Form (props){


const [todo, setTodo] = useState("");
  function handleSubmit(e) {
    e.preventDefault();
    addTodo(todo);
    setTodo("");
  }
  
  
  function handleChange(e) {
    setTodo(e.target.value);
  }

  function addTodo(todo) {
    const newTodo = { id: "id", title: todo, completed: false };
    setTodo([...todosList, newTodo]);
  }
  

 

  
  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <form   onSubmit= {handleSubmit}>
        <input
          className="new-todo"
        
          placeholder="What needs to be done?"
          value={todo}
          onChange={handleChange}
          autoFocus
        />
        </form>
      </header>
      <App todos={todosList} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <button className="clear-completed">Clear completed</button>
      </footer>
    </section>
  );
}