import React, { useState } from "react";
import todosList from "./todos.json";
import Todo from "./Components/Todo/Todo";

function App() {


  const [todo, setTodo] = useState("");
  function handleSubmit(e) {
    e.preventDefault();
    addTodo(todo);
    setTodo("");
  }
  
  
  function handleChange(e) {
    setTodo(e.target.value);
  }

  function addTodo(todo) {
    const newTodo = { id: "id", title: todo, completed: false };
    setTodo([...todosList, newTodo]);
  }
  

 

  
  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <form   onSubmit= {handleSubmit}>
        <input
          className="new-todo"
        
          placeholder="What needs to be done?"
          value={todo}
          onChange={handleChange}
          autoFocus
        />
        </form>
      </header>
      <TodoList todos={todosList} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <button className="clear-completed">Clear completed</button>
      </footer>
    </section>
  );
}



function TodoList(props) {
  const [todo, setTodo] = useState(todosList);
    function addTodo(todo) {
    const newTodo = { id: "id", title: todo, completed: false };
    setTodo([...todosList, newTodo]);
  }
  
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <Todo title={todo.title} completed={todo.completed} id={todo.id} />
        ))}
      </ul>
    </section>
  );
}

export default App;
